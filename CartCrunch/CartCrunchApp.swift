//
//  CartCrunchApp.swift
//  CartCrunch
//
//  Created by Marlon Johnson on 5/2/23.
//

import SwiftUI

@main
struct CartCrunchApp: App {
    let persistenceController = PersistenceController.shared

    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(\.managedObjectContext, persistenceController.container.viewContext)
        }
    }
}
